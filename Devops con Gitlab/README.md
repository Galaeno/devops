# Duración del curso: 06:15:39

# 1. Introducción al curso (04:21)
Todas las computadores que tienes a tu alrededor corren software que nosotros como desarrolladores escribimos, sin embargo las prácticas tradicionales han hecho que tanto las entregas, seguridad, entre otras no sean óptimas o no podamos movernos a la velocidad que nos gustaría cuando automatizamos.

DevOps es una serie de prácticas culturales, pero también de herramientas que nos permiten llevar software de manera ágil a los usuarios. Cuentas con herramientas como manejo de repositorio(Github o Gitlab), CI o CD(CircleCI, TravisCI o Gitlab), manejo de proyectos (Trello, Asana o Gitlab).

Gitlab es una herramienta que nos permite utilizar las buenas prácticas del DevOps en un solo lugar.

# 2. ¿Qué es Devops? (08:45)
Si desarrollas software tú debes ser el encargado y el responsable de que ese programa corra muy bien en producción. No existe el "corre en mi máquina"

Un desarrollador escribe el código que alimenta a la aplicación, se encarga de la seguridad y que no existan huecos de seguridad. También empaqueta el código mediante contenedores para correr en la infraestructura especificada.

Un DevOps se asegura de que el código corra configurando servidores, redes, infraestructura del internet y más. Se encarga de que las máquinas funcionen y puedan dar el servicio escrito por los desarrolladores.

Un DevOps es un superhéroe, se encarga que desde el momento en que se escribe el código hasta cuando corre en el dispositivo de nuestros clientes, todo el proceso sea seguro, automatizado y con prácticas de calidad.
  - Pruebas automatizadas: Las unit test que deberías estar escribiendo.
  - Continuous Integration: Automatiza los procesos de calidad.
  - Continuous Delivery: Se trata de enviar continuamente código a nuestros cliente.
  - Monitoreo y logging: Es la forma de saber qué es lo que sucede dentro de nuestro programa y detectar problemas oportunamente.
  - Microservicios: Separa las funcionalidades de la aplicaciones en servicios independientes.
  - Comunicación y colaboración: Muy importante en la cultura DevOps.

# 3. El ciclo de vida del Devops (08:21)
El modelo de DevOps no es una receta de cocina donde sigues las instrucciones y obtendrás un resultado. Este modelo es iterativo, un infinite loop y si lo detenemos nuestra compañía muere.

El modelo de DevOps se entiende como un loop donde existen diferentes etapas y consideraciones que atienden algunas etapas en especifico.
  - Plan: Acá definimos las labores, los requerimientos que se necesitan para implementar en nuestra plataforma y para esto usamos herramientas como issues o boards.
  - Create: Es escribir el código necesario para resolver el problema de negocio que tenemos. Todo este código podemos tenerlo en un solo lugar para colaborar y se hace uso de repositorios, también branchs o tags.
  - Verify: Se corren nuestras pruebas automatizadas donde definimos las reglas a probar.
  - Package: Empaquetamos nuestro código para correr en una infraestructura determinada. Normalmente se hace en un contenedor de Docker.
  - Release: Una nueva versión de nuestro código y llega a producción.
  - Configure: Se puede cambiar nuestro cluster de kubernetes, mandar instrucciones para manejar el nuevo estado de nuestra aplicación.
  - Monitor: Cómo nuestro código esta funcionando, qué tipo de performance ocurre en los dispositivos de nuestros clientes.

![Ciclo de Vida](./ciclo vida devops.png)

# 4. Introducción a Gitlab (09:30)
Gitlab es una compañía que realiza un proyecto open source, un producto especializado en el ciclo de vidas del DevOps.
  - Administración: Nos da opciones de autenticación, autorización, analytics y self hosted gitlab.
  - Planificación: Nos da issues, milestones, burndown charts, discusiones, boards, service desk, to-dos, etc.
  - Creación: Generar proyectos, grupos, repositorios de código, merge request, integración y automatización.
  - Verificación: Correr pruebas automatizadas, calidad del código, review apps y code coverage.
  - Empaquetación: Container registry, paquetes privados.
  - Distribución(release): Deployment strategies, ambientes, Gitlab pages, feature flags.
  - Configuración: Existe la posibilidad de que debas cambiar la configuración de tu sistema. Auto DevOps, integración con Kubernetes, Knative serverless, manejo de secreto, chatops.
  - Monitoreo: Prometheus, Jaeger, Sentry.
  - Seguridad: Container scanning, dependency scanning, dynamic security testing, static security testing, manejo de licencias, security dashboard.
  - Defensa: Gitlab está trabajando en herramientas para esta etapa como Firewalls, threat detection, data loss prevention y más.

# 5. Gitlab vs Github (03:25)
Github y Gitlab son plataformas que comparten algunas funcionalidades aunque tienen objetivos diferentes.

Github nace como un repositorio en la nube colaborativo basado en Git para permitir participar en proyectos. En Github una persona fuera de una organización puedan colaborar en estos proyectos, es como una red social de programadores. Fue adquirido por Microsoft.

Gitlab nació como una versión open source de Github y a lo largo del tiempo ha encontrado un nicho en agregar herramientas y generar integraciones directamente al producto. Tiene una visión de que su cliente principal es alguien especializado en DevOps

# 6. Autenticación (12:19)
Gitlab te ofrece varias formas de autenticación tales como:
  - Username y Password: Login
  - Two factor authentication(2FA): OTP y FIDO U2F
  - SSH Key: Push code: 
    - ssh-keygen -t rsa -b 4096 -C "gonzalomhenriquez86@gmail.com"
    - Copiar contenido del archivo generado (.ssh/<app>_rsa.pub)
    - Pegarlo en gitlab en sección SSH Keys de la configuración

# 7. Grupos (08:35)
Los grupos te permiten compartir recursos entre varios miembros del equipo y organizar la forma en la que trabajas.
  - Agrupar proyectos: dónde va a vivir nuestro código y los recursos asociados.
  - Otorgar permisos de acceso: qué usuarios de qué equipo van a poder acceder a los recursos de la compañía.
  - Compartir recursos: Si tienes cluster de Kubernetes, templates o runners para correr el CI lo puedes compartir entre varios grupos.

Los grupos se utilizan en Gitlab a través de los Namespaces que nos dan una url única para nuestro usuario, grupo y subgrupo.

# 8. Autorización (05:31)
Existen diferentes formas de autorizar un usuario dentro de un grupo y estos mismos modelos se utilizan para los proyectos:
  - Guest: Es Read-only, solo tiene permisos de lecturas. No puede modificar código, crear o comentar issues.
  - Reporter: Solo puede crear y comentar en los issues. No puede añadir código.
  - Developer: Puede añadir código, también da acceso a los Pipelines de CI, branchs y más, pero no da la capacidad de agregar nuevos miembros.
  - Owner / Maintainer: Eres Owner cuando creas un proyecto y Maintainer cuando alguien más te da permisos para administrar ese proyecto.

# 9. Auditoría (03:14)
Gitlab nos permite generar rastro de auditoría cuando se realizan cambios que pueden modificar el grupo, el proyecto o la instancia.

# 10. Proyectos (07:41)
Los proyectos tienen tres componentes fundamentales:
  - Issue tracker: No es uno genérico para cualquier compañía, es específico para equipos que desarrollan software y adentro encontrarás conceptos como milestone, estimados de tiempo y más.
  - Code repository: Es el lugar centrar que nos va a permitir compartir y colaborar alrededor del código.
  - Gitlab CI: Nos muestra la posibilidad de automatización con la inclusión de Continuous Integration.

# 11. Tipos de desarrollo (06:19)
Las principales diferencias entre Agile y Waterfall es que en el primero encontramos un proceso iterativo y en el segundo utilizamos un proceso previamente definido. En Agile estamos realizando sprints, pequeños esfuerzos de trabajo para al final tener un entregable y mandarlo a producción.

En Waterfall tenemos nuestro entregable hasta el final del proyecto, como crear una casa de manera modular.

# 12. Planificación en Gitlab-Issues (12:08)
Los issues son el punto donde inicia una conversación sobre el código.
Los issues permiten:
  - Discutir la implementación de una nueva idea.
  - Sugerir propuestas de features.
  - Hacer preguntas.
  - Reportar bugs y fallos.
  - Obtener soporte.
  - Planear nuevas implementaciones de código.

Puedes añadir templates a tus issues para poder estandarizar la forma en la que se abren, podemos incluirlo de cualquier tipo. Debes crear un archivo o una estructura de carpeta como:
```
.gitlab/issue_templates/Bug.md
```

# 13. Planificación en Gitlab-Etiquetas (07:30)
El siguiente paso es clasificar los issues basados en etiquetas, suele salirse de control la forma en la que se reportan y una manera sencilla de organizar es con etiquetas.

Las etiquetas nos permiten:
  - Categorizar issues o merge request con títulos descriptivos.
  - Filtrar y buscar en Gitlab
  - Seguir temas a través de notificaciones.

# 14. Planificación en Gitlab-Pesos
Uno de los puntos más complejos del desarrollo de Software es la estimación. En esta clase me gustaría abordar qué herramientas Gitlab nos ofrece para poder estimar la cantidad de trabajo que un issue requiere, qué ventajas tiene agregar pesos a los issues y algunas de las buenas prácticas relacionadas con este ejercicio.

Gitlab ofrece la funcionalidad de agregar pesos a los issues. Estos pesos se representan de manera numérica (con el límite de que los números deben poderse representar en 4-bytes). Los pesos aparecen en el menú derecho del Issue, junto al nombre del issue en la lista de issues y sirven para determinar la cantidad de trabajo ejecutado en los Burndown Charts de los Milestones.

# 15. Planificación en Gitlab-Milestones (07:22)
Los Milestones permiten agrupar issues para completarlos en un tiempo determinado.
  - Milestone como agile sprint
    – Sprint 5
  - Milestone como release
    – V1.1.2

Brundown chart nos permite determinar qué tan avazando vamos dentro de un sprint y nos permite tomar acciones cuando todavía es relevante.

# 16. Planificación en Gitlab-Boards (06:25)
Los boards son una forma de visualizar los flujos de trabajo, de ver quién está trabajando en qué issues y son unas de las herramientas más importantes que existen dentro de Gitlab.
  - Se puede utilizar para Kanban o Scrum.
  - Une los mundos de los issue tracking y Project managment.

# 17. Planificación en Gitlab-Service Desk (08:34)
El Service Desk es la capacidad que te da Gitlab de abrir issues a través de correo electrónico.
  - Permite dar soporte a través de email a tus clientes directamente desde Gitlab.
  - Permite que el equipo no técnico reporte bugs o abra issue sin necesidad de que tengan una cuenta de Gitlab.
  - Cuando se activa el servicio, se genera un email único para el proyecto.

# 18. Planificación en Gitlab-Quick actions
Las Quick Actions son atajos textuales para acciones comunes en issues, epics, merge request y commits que normalmente son ejecutadas a través de la UI de Gitlab. Los comandos se pueden agregar al momento de crear un issue o un merge request o en los comentarios de los mismos. Cada comando debe incluirse en una línea separada para que se detecten y ejecuten. Una vez ejecutados, los comandos se retiran del texto y no pueden verse en el comentario o descripción del issue.

![Gitlab-Quick actions 1](./Gitlab-Quick_1.jpg)
![Gitlab-Quick actions 2](./Gitlab-Quick_2.jpg)

Estos son tan sólo unos ejemplos de los Quick Actions que Gitlab ofrece. Gitlab añade Quick Actions todo el tiempo, por lo que te recomiendo familiarizarte con la documentación y consultarla constantemente. Aquí la lista completa: https://docs.gitlab.com/ee/user/project/quick_actions.html

# 19. Inicialización del repositorio (06:50)
Muestra como se inicializa un repositorio local y se relaciona con el remoto en git.

# 20. Merge requests (12:24)
Los Merge Requests son la puerta de entrada a nuestro código, es el momento en donde definimos que un cambio sugerido por otra persona será unido a nuestra rama master o rama principal. Para tomar esta decisión se necesita mucha información: si los cambios fueron correctos, resuelven el issue, si surgen problemas de seguridad, si mejora nuestro performance.

El título del merge requests tiene prefijado WIP que significa Work in Progress.

# 21. Profundizando en Merge requests (09:24)
Explica más el detalle de todas las caracteristicas que tiene y las oepraciones que se pueden realizar con un merge request.

# 22. Continuous Integration-CI (02:59)
El Continuous Integration es una práctica en la que los desarrolladores envían sus cambios a un repositorio central, lo cual detona builds y pruebas automatizadas.
  - Ayuda a encontrar bugs de manera oportuna
  - Aumenta la velocidad de los releases
  - Automatiza el pipeline que lleva código desde la computadora del desarrollador hasta el dispositivo del cliente.

# 23. Gitlab CI (10:14)
Gitlab CI es el hub central de automatización de Gitlab, es el pedazo que podemos configurar libremente para generar las automatizaciones necesarias para que nuestro flujo de trabajo requiera poca o ninguna interacción humana.
  - Continuamente construye, prueba y despliega cambios pequeños al código.
  - Se configura con el archivo gitlab-ci.yml

También nos permite realizar Continuous Delivery y Continuous Deployment.

# 24. Automatizacion con GitLab Cl (11:54)
Muestra como crear el archivo gitlab-ci.yml para el proyecto.

# 25. Validacion de la configuracion con GitLab Cl (09:19)
Muestra una herramienta para validar el archivo gitlab-ci.yml:
```
<URL del proyecto>/-/ci/lint
```

# 26. gitlab-ci.yml
El archivo .gitlab-ci.yml sirve para configurar el comportamiento de Gitlab CI en cada proyecto. En el archivo define la estructura y el orden de los pipelines y determina qué ejecutar con el Gitlab runner y qué decisiones tomar cuando condiciones específicas se cumplen (como cuando un proceso falla o termina exitosamente).

El archivo tiene muchas opciones de configuración, pero aquí nos vamos a enfocar en tres: image, stages y jobs.

La primera opción de configuración es image. image nos sirve para determinar qué imagen de Docker vamos a utilizar para ejecutar el runner. Hay que recordar que, en su nivel más básico, los trabajos de CI son simplemente automatización de scripts. Con esto en mente, tenemos que determinar qué ambiente necesita nuestro script para correr de manera exitosa. ¿Necesitas instalar dependencias desde NPM y ejecutar scripts de package.json? Entonces es muy probable que la imagen de Node te sirva como base. Quizá necesitas correr pruebas unitarias de una aplicación de Python e instalar dependencias desde PyPi; entonces deberías instalar la imagen de Python.

Al final del día, el keyword image nos permite “preinstalar” los paquetes que nuestro script necesitará para correr: desde sistema operativo y lenguajes de programación, hasta software específico como bases de datos.

Un último punto, image puede utilizarse de manera global o por cada job que ejecutemos. Es decir, si nuestro proyecto lo requiere podemos utilizar una imagen de Node para un job y otra de Ruby para otro.

```yml
# gitlab-ci.yml
image: node:11.1.0

# ó

job1:
  image: python:3.7
…
```

Por su parte, los stages nos permiten definir las etapas que atravesará nuestro pipeline cuando corra. Cada stage (etapa) puede contener uno o más jobs que correrán en paralelo. Los stages permiten agrupar los jobs que pertenezcan a una categoría y permiten crear diversas dependencias entre cada job. Un ejemplo de un pipeline multietapa sería el siguiente:

Install -> Test -> Build -> Deploy

Dicho pipeline lo podríamos configurar en .gitlab-ci.yml de la siguiente manera:

```yml
# gitlab-ci.yml

stages:
  - install
  - test
  - build
  - deploy

job1:
  stage: install
…
```

Es importante recordar que para configurar nuestros pipelines de manera correcta, tenemos que declarar el nombre de nuestras etapas como una lista bajo el keyword stages y luego indicar en cada job a qué etapa pertenece con el keyword stage.

Por último, los jobs son los encargados de ejecutar los scripts de automatización en cada etapa. En este sentido, un job puede tener casi cualquier nombre (aunque siempre debes intentar nombrar de acuerdo a la función de lo que estás nombrando) y debe contener un script que se ejecutará. Los jobs se ejecutan por los runners cuando se encuentran disponibles. Gran parte de la configuración adicional de los jobs está relacionada con las condiciones sobre las cuales se debe ejecutar y que artefactos exporta para otros jobs en el pipeline.

```yml
# gitlab-ci.yml

job1:
  script:
    - echo “Hello, world”
    - npm install
    - echo “etc.”

…
```

Recuerda que el archivo .gitlab-ci.yml es fundamental para configurar nuestro CI, pero nuestro IDE no posee las reglas para determinar si su estructura es válida. De hecho, si tienes el tiempo, crear un plugin de VS Code o de VIM para estos efectos sería una gran idea. Por esto, es importante validar nuestra configuración con la herramienta de linting de Gitlab. La puedes encontrar en cada uno de tus proyectos si configuras la siguiente URL con tus datos.

https://gitlab.com/group/proyect/-/ci/lint

# 27. Gitlab pages (04:26)
Sirven para alojar tu web estatica en gitlab.

# 28. Implementando Gitlab pages (13:13)
Gitlab Pages nos otorga ciertas funcionalidades como:
  - Hosting estático para servir nuestro website.
  - Integración con Gitlab CI.
  - Dominios personalizados.

# 29. ¿Qué es el Desarrollo Ágil? (02:51)
El Desarrollo ágil de software viene del concepto básico de agilidad, la capacidad de responder a cambios.

En el “Agile Manifest” la biblia del desarrollo ágil eligen el término por su adaptabilidad.

Pero… ¿qué es el desarrollo ágil?
El desarrollo ágil abarca todo un set de frameworks y prácticas basadas en los 12 principios del manifiesto ágil, en los que tú eliges cuáles de estos principios son los mejores para tu desarrollo.

# 30. Gitlab autodevops (06:26)
Gitlab autodevops es una solución que te permite generar un flujo de devops inmediato con la creación del proyecto que incluye todas las mejores prácticas.
- Features:
  - Auto build
  - Auto test
  - Auto Code Quality
  - Auto SAST
  - Auto dependency scanning
  - Auto container scanning
  - Auto review apps
  - Auto Dast
  - Auto Deploy
  - Auto performance
  - Auto testing
- Prerequisitos
  - Gitlab Runner
  - Kubernetes
  - Base domain
  - Prometheus.

# 31. Implementando GitLab autodevops (09:39)
Pasos para implementar autodevolps:
- Agregar cluster de Kubernetes, aparece boton en el home del proyecto
- Para agregar el cluster, se deberá tener una cuenta en google cloud, o aws, o bien, si ya se tiene un cluster, usar ese. La url de google cloud es: https://console.cloud.google.com
- Configuración:
  - Kubernetes cluster name: el nombre del cluster que se quiere usar, puede ser cualquiera
  - Environment scope: Dejar el default *
  - Google Cloud Platform project: Muestra los projectos que se tiene en google cloud.
  - Zone: Elegir en que zona va a estar el cluster, default: us-central1-a
  - Number of nodes: Numeros de servidores a usar, default 2
  - Machine-type: Tipo de maquina a usar. Default: n1-standard-2
  - RBAC-enabled cluster: Dejar habilitado
- Luego de que se generó el cluster, comienza a instalar las aplicaciones que usará kubernetes:
  - Helm Tiller: Es el manejador de packetes de kubernetes (va a habilitar las demas apps)
  - Ingress: Es un node balancer en kubernetes. Me muestra luego la ip que se usará.
  - Cert-Manager: Son certificados de seguridad gratuitos
  - Prometheus: Monitorea la salud del cluster y su performance.
- La IP estatica que nos da Ingress, tenemos que decirle a google que no queremos que sea efimera, que la vamos a usar:
  - Ir a Google Cloud
  - Ir al menu de VPC Networks ->  External IP addresses
  - Buscar la IP que nos dio Ingress click en type y seleccionar estatica, dandole nombre y descripción
- Con esto ya está configurado autodevops, pero falta habilitarlo.

# 32. Habilitando autodevops (13:39)
Pasos para habilitar autodevops:
- Ir a Settings -> CI / CD
- Expandir Autodevops
- Chequear el campo Default to Auto DevOps pipeline
- Deployment strategy:
  - Continuous deployment to production
  - Continuous deployment to production using timed incremental rollout
  - Automatic deployment to staging, manual deployment to production
- Luego de guardar, ir a pipelines y verificar que se creó el pipeline principal (lo genera automaticamente los jobs)
- Hay que añadir un dominio para acceder a los servicios que se van a mandar a producción:
  - Operations -> Kibernetes -> Nusetro clouster
  - Base domain: <nuestra_ip>.nip.io .nip.io es un proxy, si ya se tiene un dominio base, usar ese


# 33. Introducción a contenedores
Uno de los conceptos que han modificado de manera radical la forma en que desarrollamos software son los contenedores. Estoy seguro que si te mueves en el mundo de la tecnología has escuchado de Docker y Kubernetes. En esta clase vamos a investigar un poco más qué son los contenedores, cómo se comparan con otras tecnologías (como las máquinas virtuales) y cuáles son algunas de las opciones que tenemos para comenzar a utilizarlos.

Tanto los contenedores como las máquinas virtuales tienen un objetivo común: aislar a la aplicación y sus dependencias en una unidad que pueda ejecutarse en cualquier lugar. Más aún, tanto los contenedores como las máquinas virtuales eliminan la necesidad de proveer a nuestros servicios con hardware físico. Esto significa que se pueden utilizar de manera más eficiente los recursos computacionales que tenemos a nuestra disposición. La gran diferencia entre ambos es el enfoque arquitectónico que toman.

Una máquina virtual es esencialmente una emulación de una computadora con la capacidad de ejecutar procesos y programas. Las máquinas virtuales requieren de un hypervisor para poderse ejecutar sobre una host machine o directamente sobre “el metal”.

El hypervisor es un componente de software o hardware que permite a la host machine compartir recursos (RAM y procesadores) entre varias máquinas virtuales. Esto es importante, porque si el sistema está corriendo una aplicación de cómputo intensivo, se le pueden asignar más recursos que otras aplicaciones corriendo en el mismo sistema.

En este sentido, la máquina virtual que corre sobre un hypervisor se le conoce como la guest machine (máquina invitada). Esta guest machine contiene todo lo necesario para correr la aplicación (por ejemplo, binarios y librerías de sistema). También contiene toda una pila de hardware virtualizado (adaptadores de redes, almacenamiento, CPU, etc.). Desde el interior, la guest machine se comporta exactamente como una unidad de cómputo. Desde afuera, sabemos que está utilizando recursos compartidos que le otorga la host machine.
![Contenedores 1](./Introducción a contenedores_1.jpg)

A diferencia de las máquinas virtuales –que proveen virtualización de hardware–, los contenedores proveen virtualización al nivel del sistema operativo (al abstraer el user space).

A primera vista, los contenedores se parecen mucho a las máquinas virtuales. Por ejemplo, tienen un espacio dedicado al procesamiento, pueden ejecutar comandos como root, tienen interfaces de red privadas y direcciones IP, permiten configurar reglas de ruteo y iptables, tienen la posibilidad de montar file systems, etc.

La gran diferencia es que los contenedores comparten el kernel de la host machine con otros contenedores.
![Contenedores 2](./Introducción a contenedores_2.jpg)

El diagrama anterior muestra que los contenedores sólo empaquetan el user space, y no el kernel o hardware virtual como lo hace una máquina virtual. Podemos ver que toda la arquitectura del sistema operativo se comparte entre todos los contenedores. Lo único que se crea cada vez son la aplicación y las librerías y binarios. Esto es lo que hace a los contenedores tan ligeros.
Existen muchas tecnologías para crear contenedores, pero la más importante hoy en día es Docker. Docker es un proyecto open source que utiliza tecnologías de Linux para crear la abstracción de un contenedor. Sin embargo, esta no es la única tecnología en el mercado. Empresas como Google, llevan más de una década utilizando contenedores. Otros contendientes son: Solaris Zones, BSD jails, LXC, etc. Entonces, ¿qué fue lo que hizo que Docker tomara tanta relevancia?

Lo primero, es la facilidad de uso. Docker permite que cualquier usuario (desarrolladores, sysadmins, etc.), pueda empaquetar su aplicación rápidamente en su computadora personal y la misma aplicación puede correr ahora en cualquier nube pública, datacenter privado o directamente en hardware.

Una segunda ventaja es su velocidad. Los contenedores son bastante ligeros (comparados con las máquinas virtuales), pues son simplemente ambientes contenidos corriendo en el kernel. Las imágenes de Docker se crean en segundos, mientras que las máquinas virtuales toman más tiempo pues necesitan inicializar un sistema operativo completo cada vez.

Por último, Docker cuenta con el Docker Hub lo que permite compartir imágenes con mucha facilidad. Docker Hub tiene miles de imágenes públicas que han sido creadas por la comunidad para satisfacer casi cualquier necesidad. Puedes escoger entre decenas de sistemas operativos, lenguajes de programación y librerías para utilizar como base en tu aplicación.

Los contenedores son importantes en el mundo de Gitlab pues nos permiten aprovechar integraciones con clusters de Kubernetes y Gitlab Container Registry, y utilizar AutoDevOps para crear flujos inmediatos de DevOps que podrían tomarnos semanas si hiciéramos una integración ad hoc. Estas herramientas asumen que utilizas Docker y contenedores para empaquetar tu aplicación.

Si quieres aprender esta tecnología a profundidad, te recomiendo que tomes el curso de Fundamentos de Docker aquí mismo en Platzi: https://platzi.com/cursos/docker/

# 34. Gitlab container registry (06:43)
Gitlab container registry permite almacenar imágenes de Docker para uso posterior. En un caso tradicional, cada vez que el CI tiene un build exitoso, una nueva imagen se envía al container registry

# 35. Introducción a DevSecOps (06:28)
En el pasado, el equipo de Seguridad actuaba aislado y actuaba únicamente al final del proceso de desarrollo, un flujo de waterfall. Esto funcionaba porque eran ciclos de desarrollo que llevaban meses o años.

DevSecOps significa pensar en la seguridad de la aplicación a lo largo del proceso, desde el principio. Se trata de automatizar la seguridad e incluirla en el ciclo de vida de la aplicación (no más seguridad externa y en perímetros)

# 36. Firmas de seguridad (10:18)
GPG permite identificar, sin lugar a dudas, de quién proviene un commit; añade una capa adicional de seguridad a Git para prevenir "caballos de troya".

Gitlab despliega un banner junto a los commits para mostrar que dichos commits están verificados.

# 37. Pruebas estáticas de seguridad (08:38)
Las pruebas estáticas de seguridad analizan nuestros archivos buscando patrones inseguros de código.
  - Crean un reporte que es añadido como widget al merque request
  - Utilizan la imagen de Docker SAST de Gitlab

Tipos de vulnerabilidades
  - Critical: Existe un falla de código que da acceso de root o a los sistemas sin necesidad de ingeniería social. Debes atenderla de inmediato.
  - High: Si se explota este tipo de vulnerabilidad estamos en riesgo de perder datos. Es difícil de explotar.
  - Medium: El hacker va a tener que realizar trabajo adicional para obtener el acceso deseado.
  - Low: No representan un riesgo de pérdida de datos.
  - Unknow: No han sido clasificadas todavía y debes evaluarlas una por una.


gitlab-ci.yml
```yml
include:
  template: SAST.gitlab-ci.yml
```

# 38. Escaneo de contenedores (03:39)
Muestra que en los MR se pueden ver el resultado de los escaneos de los contenedores (imagenes):
gitlab-ci.yml
```yml
include:
  template: Container-Scanning.gitlab-ci.yml
```

# 39. Escaneo de dependencias (05:35)
El Dependency scanning analiza estáticamente las dependencias del proyecto para encontrar vulnerabilidades. Puede generar un reporte que se añade al merge request y utiliza la imagen de Docker Dependency Scanning de Gitlab:
gitlab-ci.yml
```yml
include:
  template: Dependency-Scanning.gitlab-ci.yml
```

# 40. Pruebas dinámicas de seguridad (06:37)
Las pruebas dinámicas de seguridad asumen que es un atacante externo y la aplicación es un blackbox para así correrle ciertas pruebas.
  - Utiliza OWASP ZAP proxy y ZAP baseline.
  - Corre análisis pasivo.
  - Genera un reporte que puede ser verificado en el merge request.
gitlab-ci.yml
```yml
include:
  template: DAST.gitlab-ci.yml
```

# 41. Gitlab security dashboard (04:36)
El Gitlab security dashboard es un hub centralizado de información donde tienes visibilidad de las vulnerabilidades que actualmente están corriendo en producción.

Permite acceder rápidamente a los riesgos detectados y aceptados para el ambiente de producción, permite marcar una vulnerabilidad como inválida o no aplicable, genera vínculos a los reportes de seguridad externos para entender mejor una vulnerabilidad.

# 42. Continuous Delivery (CD) (08:04)
O también llamado Continuous Deployment, muestra las estrategias a usar para realizar actualizaciones automaticas en ambientes de producción.

# 43. Ambientes (08:09)
Los Ambientes se suelen utilizar para determinar si el código escrito cumple con las expectativas del negocio y los requisitos impuestos con antelación para que así las personas puedan aprobarlos o no.
  - Permiten realizar pruebas en diferentes ambientes antes de enviar el código a nuestros usuarios.
  - Se integran con Gitlab CI para hacer realidad el Continuous Deployment.
  - Gitlab lleva el historial de todos los deployments que se han realizado a un ambiente específico.
  - Permiten verificar que el Deployment process se encuentre intacto y da la oportunidad de hacer QA

Tenemos algunos tipos como:
  - Estáticos
  - Dinámicos
  - Protegidos

# 44. Review apps (13:36)
Las Reviews apps permiten ver los cambios de un feature branch al activar un ambiente para ejecutar el código con cada merge request.
  - Los diseñadores y los product managers pueden ver los cambios sin necesidad de levantar un ambiente local en sus computadoras
  - Cuando el merge request se aprueba y el feature branch se borra, se detiene el review app y se destruye la infraestructura
  - Completamente integrado con GitlabCI y merge request.
```yml
review:
  stage: review
  script:
    - # script para crear el ambiente y hacer deploy
  environment:
    name: review/$CI_BUILD_REF_NAME
    url: http://$CI_BUILD_REF_SLUG.$APP_DOMAIN
    on_stop: stop_review
  only:
    - branches
  except:
    - master

stop_review:
  stage: review
  script:
    - # script para destruir el ambiente
  variables:
    GIT_STRATEGY: none
  when: manual
  environment:
    name: review/$CI_BUILD_REF_NAME
    action: stop
```

# 45. Estrategias de Distribución
Uno de los temas que más ha cambiado en los últimos años en el mundo de la Ingeniería de Software es la velocidad a la que se distribuyen cambios en nuestros aplicaciones. Los equipos de desarrolladores distribuyen cambios más temprano y más rápido que antes. Mientras que antes los ciclos naturales tomaban meses o años, hoy en día los cambios suceden varias veces al día. Los beneficios son claros:
  - El tiempo requerido para comercializar productos o servicios se disminuye drásticamente.
  - Los clientes obtienen el valor del producto en menos tiempo.
  - La retroalimentación de los clientes también fluye a velocidades aceleradas, lo que permite al equipo de desarrollo iterar de manera más rápida.
  - La moral del equipo aumenta, pues pueden ver el fruto de su trabajo en producción.

Sin embargo, no todo es felicidad. Con este tipo de estrategias aceleradas, existe un mayor riesgo de introducir cambios que pueden afectar negativamente la experiencia del usuario; o peor aún, traer downtime a nuestro sistema. Por eso, es importante incluir prácticas que permitan minimizar el riesgo de que lo anterior se materialice.

Big bang deployment
Como lo sugiere su nombre, los despliegues de Big Bang, actualizan todas las partes del sistema en una sola barrida. Esta estrategia encuentra sus orígenes en los días en que el software se distribuía en medios físicos y el cliente lo instalaba manualmente en su máquina.

Este tipo de despliegues requieren que el negocio ejecute una enorme cantidad de pruebas durante una fase específica del desarrollo, antes de aprobar el despliegue. Este tipo de pruebas, normalmente se asocian con el modelo waterfall en el que el desarrollo se ejecuta en etapas secuenciales.

Las aplicaciones modernas tienen la ventaja de poderse actualizar automáticamente, en el cliente o el servidor, por lo que este tipo de estrategias han sido casi completamente abandonadas por equipos que siguen las metodologías ágiles.

Rolling deployment
Los rolling deployments, o despliegue en fases, tienen la ventaja de mitigar algunas de las desventajas de los big bang deployments. Esto es así, porque disminuye el riesgo de downtime al desplegar la aplicación a lo largo del tiempo.

Es importante resaltar que el despliegue consiste en reemplazar una versión de la aplicación con otra en fases; de tal manera que existe un tiempo en el que ambas aplicaciones pueden existir. En el caso de un despliegue a Kubernetes, por ejemplo, el reemplazo consiste en destruir el contenedor con la versión anterior y descargar la última versión de la imagen desde el container registry en el cual la alojemos.

Y es aquí donde se alcanza a ver otra ventaja de contenerizar nuestra aplicación: que los rollbacks resultan ser muy sencillos, cuando antes (en el modelo del Big Bang) resultaban imposibles. Hacer rollback es tan sólo destruir de nueva cuenta el pod, y descargar la versión previa (o cualquier otra versión que queramos) desde nuestro container registry.

Blue Green deployment
Esta estrategia, también conocida como A/B deployment, consiste en tener dos ambientes de producción paralelos (uno llamado blue y el otro llamado green) en el cual se despliegan las nuevas versiones de las aplicaciones de manera alternativa. Es decir, si blue tiene instalada la V1 de nuestra aplicación, entonces green tendrá instalada la V2, y cuando se despliegue se la siguiente versión (V3) se utilizará el ambiente blue, nuevamente. ¿Dónde se desplegará el ambiente V4? En green, por supuesto; y la secuencia alternativa continúa indefinidamente.

Una de las ventajas de esta estrategia es que facilita realizar un rollback a la versión anterior, de manera sencilla, cuando nuestra aplicación no se encuentra habilitada para trabajar dentro de contenedores. En caso de que exista una falla en la nueva versión, simplemente se rutea al ambiente previo.

Es importante mencionar, que únicamente el ambiente de la capa de la aplicación se replica. Las bases de datos, al igual que el almacenamiento de archivos binarios (fotos, imágenes, vídeos, por ejemplo), son compartidas por ambos ambientes.

Existe otra modalidad de los despliegues blue green que se conoce como canary deployment. El canary deployment en lugar de rutear todo el tráfico de inmediato, se utiliza una aproximación incremental. Es decir, se comienza a rutear a la nueva versión progresivamente. Por ejemplo, 25% 50% 75% 100% ó 33% 66% 100%, etc.

Una de las ventajas de adoptar esta estrategia es que se puede probar la nueva versión con un subconjunto de los usuarios para determinar si se encuentra estable, y en caso de confirmarse, se rutea todo el tráfico al ambiente green o blue.

Distribución en Gitlab
En Gitlab, cuando utilizamos AutoDevOps, podemos configurar nuestra estrategia de despliegue a través de opciones predeterminadas en el UI o a través de variables de ambiente de Gitlab CI.

Las variables que podemos configurar son las siguientes:
STAGING_ENABLED activa el ambiente staging cuando se le asigna el valor 1.
CANARY_ENABLED activa el ambiente canary cuando se le asigna el valor 1.
INCREMENTAL_ROLLOUT_MODE define la forma en el que el despliegue incremental se realizará. Acepta los valores manual y timed.

Este es el resultado de la algunas combinaciones de configuración:

Sin la declaración de INCREMENTAL_ROLLOUT_MODE ni STAGING_ENABLED.
![Estrategias de Distribución](./Estrategias de Distribución 1.jpg)

Sin la declaración de INCREMENTAL_ROLLOUT_MODE, pero con STAGING_ENABLED asignado el valor de 1.
![Estrategias de Distribución](./Estrategias de Distribución 2.jpg)

Con INCREMENTAL_ROLLOUT_MODE en modo manual, y sin STAGING_ENABLED.
![Estrategias de Distribución](./Estrategias de Distribución 3.jpg)
Con INCREMENTAL_ROLLOUT_MODE en modo manual, ySTAGING_ENABLED asignado el valor de 1…
![Estrategias de Distribución](./Estrategias de Distribución 4.jpg)

Para modificar la opción de despliegue a través de la UI, navega a Settings > CI/CD > Auto DevOps y selecciona una de la siguientes opciones:
  - Continuous deployment to production: Habilita el despliegue continuo del branch master al ambiente de producción.
  - Continuous deployment to production using timed incremental rollout: Asigna el valor timed a la variable de Gitlab CI INCREMENTAL_ROLLOUT_MODE. Esto significa que el despliegue se realizará cada vez que existe un cambio en el branch master, pero de manera incremental en lapsos de 5 minutos.
  - Automatic deployment to staging, manual deployment to production: Asigna el valor de 1 a la variable STAGING_ENABLED y el valor de manual a la variable INCREMENTAL_ROLLOUT_MODE. El resultado es que la rama master se despliega de manera continua al ambiente de staging, y se activan las acciones manuales para el despliegue a producción.

# 46. Feature Flags 
Los feature flags son una técnica poderosa, que permite a los equipos modificar el comportamiento de su sistema sin necesidad de modificar código o realizar un despliegue. Esto aumenta la velocidad del equipo sin sacrificar la estabilidad del sistema.

Sin embargo, no todo es color rosa: los feature flags introducen gran complejidad a nuestro sistema, y si no somos cuidadosos, podemos construir un sistema en el cual sea imposible escribir pruebas unitarias (por la explosión combinatoria de posibilidades que los feature flags introducen). Por eso, como regla general, debemos mantener los feature flags en un mínimo.

Ahora sí, a entrarle de lleno. Para trabajar correctamente con los feature flags, es indispensable reconocer que existen diversas categorías, y el tratamiento que le damos a cada una debe ser distinto.

Tipos de Feature Flags
La primera categoria se conoce como Release Flags. Este tipo de bandera nos permite implementar una estrategia de Continuous Delivery, dónde los diferentes features se activan de manera manual e independiente. Este tipo de estrategia es importante cuando se requiere lanzar una funcionalidad compleja que requiere estar concluida al 100% para ser lanzada o cuando se necesita coordinar un evento externo junto con el despliegue de la funcionalidad (por ejemplo, cuando el feature se lanza en coordinación con una campaña de marketing).

La segunda categoría son los Experiment Flags. Este tipo de flag se utiliza cuando nuestra aplicación permite la realización de experimentos A/B. Cada usuario de la aplicación es segmentado en cohortes y se muestran diferentes funcionalidades dependiendo del cohorte al que pertenezcan. Este tipo de flags tiene un periodo de vida muy corto, pues una vez que se ha determinado el resultado del experimento, se opta por una u otra versión, y se estandariza el uso en el sistema. También, es importante tener duraciones cortas porque cuando se corren diversos experimentos A/B de manera simultánea, existen altas posibilidades que los experimentos interfieran el uno con el otro, eliminando así la validez estadística del resultado.

Por su parte, los Ops Flags, permiten crear switches que facilitan controlar el comportamiento del sistema en runtime. Existen ocasiones, por ejemplo, en el que los sistemas reciben cargas inusuales y es necesario optimizar los recursos que tenemos disponibles para servir nuestra aplicación. En este caso, a través de feature flags es posible deshabilitar temporalmente servicios no críticos (como quizá un proceso que utiliza mucha memoria o procesamiento), para después habilitarlos una vez que la carga se haya normalizado.

Por último, los Permission Flags nos permiten habilitar funcionalidades para usuarios específicos de nuestra aplicación. Un ejemplo de lo anterior acontece cuando una compañía decide hacer dogfooding para probar internamente funcionalidades antes de habilitarlas para todos los clientes. En este caso, se puede utilizar una lista de Ids de usuarios para determinar si es necesario mostrar la funcionalidad o no.

Feature Flags en Gitlab
Gitlab ofrece la funcionalidad de manejar feature flags directamente desde la interfaz del proyecto. Detrás de bambalinas, Gitlab utiliza el proyecto open source Unleash. Unleash tiene dos componentes: un servidor (que permite definir y administrar feature flags), y librerías para el cliente para que pueda consultar el estado de un flag específico. Gitlab implementa el servidor, y deja que los desarrolladores implementen la parte del cliente, según su lenguaje de programación.

Para crear un feature flag, es necesario realizar lo siguientes pasos:

Navega al menú Operations > Feature Flags
Da click en el botón que dice New Feature Flag
Escoge un nombre y una descripción para tu feature flag
Y da click en el botón Create feature flag
![Feature Flags](./Feature Flags 1.jpg)

Un punto importante son los Environment Specs, los cuales permiten activar el feature en diferentes ambientes (por ejemplo, staging y producción). También se puede habilitar el ambiente * que aplica para todos los ambientes. Gitlab toma por default el ambiente más específico.
![Feature Flags](./Feature Flags 2.jpg)

Por último, para generar la integración del lado del cliente es necesario utilizar una de las múltiples librerías que ofrece Unleash. Para configurarlo, es necesario obtener los datos de configuración desde la interfaz de Gitlab y añadirlos al momento de inicializar nuestra librería. Para eso, navega a Operations > Feature Flags y da click en el botón Configure. En ese momento aparecerá un popup con la información que necesitas.
![Feature Flags](./Feature Flags 3.jpg)

# 47. Rollback (05:14)
Rollback es un mecanismo que nos permite regresar a la versión anterior o donde estés seguro de que la aplicación sigue funcionando. Esto con tan solo un click.
  - Gitlab ofrece la funcionalidad de "re deploy" para correr cualquier pipeline que haya sido ligado a ambiente.
  - Permite automatizar el regreso a ambientes libres de bugs.

# 48. ¿Por qué monitorear? (04:59)
Cuando el codebase era relativamente estático, operations no se preocupaba mucho del monitoreo, pero ahora con la llegada de DevOps y con cambios muy frecuentes al ambiente, el monitoreo se vuelve indispensable.

Best practices:
  - Monitorea todos los ambientes(incluyendo review apps).
  - Familiarizate con las métricas "normales" de tu aplicación.
  - Automatiza el monitoreo.
  - Comparte los datos con el resto de la organización.
    - Reportes.
    - Accesos privilegiados.
  - Monitorea aplicación, infraestructura y equipo.

# 49. Métricas de desempeño (performance metrics) (04:36)
Las métricas de desempeño nos pueden dar una idea de qué tanto está creciendo la infraestructura y la capacidad de respuesta para nuestros clientes.
  - Nos ayudan a medir el rendimiento.
  - Nos dan una idea de cómo afinar un workload a una query.

# 50. Métricas de salud (health metrics) (05:43)
Las métricas de salud nos permiten entender si nuestros dispositivos o infraestructura están a punto de fallar. Nos ayuda a decidir si debemos aumentar o disminuir nuestros recursos

# 51. Metricas de equipo (05:45)
  - Cycle analytics.
  - Lenguajes de programación utilizados
  - Commits por día del mes, de la semana y hora
  - Pipelines exitosas, fallidas y tiempo de ejecución
  - Contribuciones por persona
  - Git timeline

# 52. Rastreo de errores (12:29)
Integración con Sentry -> sentry.io para captar errores de la aplicación.

# 53. ¿Por qué desarrollar con Gitlab? (02:30)
Has podido entender cómo la automatización del proceso de desarrollo te da una ventaja competitiva en un mercado difícil, entendiste por qué devops es una filosofía y una sería de herramientas que te permiten crecer.