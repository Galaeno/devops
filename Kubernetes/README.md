# Duración del curso: 01:52:41

# Introducción a Kuberentes
## 1. Bienvenida e introducción (04:35)
¡Bienvenido al curso!

Kubernetes es la plataforma de orquestación con mayor éxito en el mercado.

Es necesario que tengas conocimiento o te sientas cómodo trabajando desde la línea de comandos o terminal y conocimientos básicos de Docker.

Objetivos:
  - Visión y entendimiento de la plataforma de Kubernetes
   Uso y buenas prácticas de las distintas herramientas
  - Workflow aplicativo sobre la plataforma de Kubernetes

Puedes compartir tus conocimientos o logros con el hashtag #PlatziK8s

Links:
- Play with Kubernetes -> https://labs.play-with-k8s.com/
- Curso de GO | Materiales -> https://platzi.com/clases/go-basico/
- Fundamentos de Docker | Materiales -> https://platzi.com/clases/docker/
- Introducción a Terminal y Línea de Comandos | Materiales -> https://platzi.com/clases/terminal/
- Play with Docker -> https://labs.play-with-docker.com/

## 2. Repaso de contenedores e introducción a k8s (09:31)
Los contenedores no son un first class citizen del kernel de Linux, un contenedor no es una entidad previamente definida. Es un concepto abstracto conformado por diferentes tecnologías que se potencian las unas a las otras y trabajando en conjunto componen esta tecnología.
  - Cgroups o Control Groups: Son los que permiten que un contenedor o proceso tenga aislado los recursos como memoria, I/O, Disco y más. Limitan los recursos del SO
  - Namespaces: Permiten aislar el proceso para que viva en un sandbox y no pueda haber otros recursos de SO o contenedores.
  - Mount Namespaces: Permite que nuestro proceso tenga una visibilidad reducida de los directorios donde trabaja.
  - Networking Namespaces: Permite que cada contenedor tenga su stack de red.
  - PID.
  - Chroot: Cambia el root directory de un proceso.

## 3. De pods a contenedores (14:17)
Docker & Kubernetes
  - Docker se encarga principalmente de gestionar los contenedores.
  - Kubernetes es una evolución de proyectos de Google Borg & Omega.
  - Kubernetes pertenece a la CNCF (Cloud Native Computing Foundation).
  - Todos los cloud providers (GCP/AWS/Azure/DO) ofrecen servicios de managed k8s utilizando Docker como su container runtime
  - Es la plataforma más extensiva para orquestación de servicios e infraestructura

Kubernetes en la práctica
  - K8s permite correr varias réplicas y asegurarse que todas se encuentren funcionando.
  - Provee un balanceador de carga interno o externo automáticamente para nuestros servicios.
  - Definir diferentes mecanismos para hacer roll-outs de código.
  - Políticas de scaling automáticas.
  - Jobs batch.
  - Correr servicios con datos stateful.

Todos los contenedores que viven dentro de un mismo Pod comparten el mismo segmento de red.

POD: Son uno o mas contenedores que viven juntos y que comparten un mismo segmento de red.

Links:
Curso de Swarm | Materiales -> https://platzi.com/clases/docker-swarm/

## 4. ¿Cómo funciona la arquitectura de red de Kubernetes? (11:39)
En esta clase el profesor Marcos Lilljedahl nos explica algo fundamental para el aprendizaje de esta tecnología: cómo funciona la arquitectura red de Kubernetes.

Kubernetes es un proyecto open source para automatizar despliegues, escalar y orquestar aplicaciones dentro de contenedores.

## 5. Modelos declarativos e imperativos (06:11)
Los control managers se encargan de estar en un loop constante de reconciliación y tratar de converger a ese estado deseado, ese es un sistema declarativo. Un sistema imperativo parece un sistema fácil de seguir y está compuesto por una serie de pasos que deben seguirse a rajatabla.
  - Kubernetes hace énfasis en ser un sistema declarativo
  - Declarativo: “Quiero una taza de té”
  - Imperativo: “Hervir agua, agregar hojas de té y servir en una taza”
  - Declarativo parece sencillo (siempre y cuando uno sepa cómo hacerlo)
  - Todo en Kubernetes se crea desde un spec que describe cuál es el estado deseado del sistema
  - Kubernetes constantemente converge con esa especificación

## 6. Visión general del modelo de red (08:27)
  - Todo el cluster es una gran red del mismo segmento
  - Todos los nodos deben conectarse entre si, sin NAT (Network Adress Translation)
  - Todos los pods deben conectarse entre si, sin NAT
  - kube-proxy es el componente para conectarnos a pods y contenedores (userland proxy/iptables)
  - Los pods trabajan a capa 3 (transporte) y los servicios a capa 4 (protocolos)

## 7. Recomendaciones
Durante este curso vamos a utilizar algunos comandos en nuestro cluster de Kubernetes para crear recursos y ejemplificar algunas situaciones de manera concreta. Esos comandos son a modo ejemplificativo y no hacen parte de nuestra aplicación de prueba.

Todo el contenido de nuestra aplicación y los manifest files que utilizamos a lo largo de las clases se encuentran en https://github.com/platzi/curso-kubernetes en sus respectivas carpetas de dockercoins y k8s.

En la clase de aplicación de prueba vamos a clonar este repositorio y trabajaremos sobre los archivos contenidos en las subsiguientes clases.

## 8. Introducción a aplicación de prueba (15:37)
Pasos para usar Kubernetes con el proyecto de prueba (dockercoins):
- La apliación tiene 5 microservicios
- La forma de acceder a otro microservicio, es en vez de asignar ip o localhost, es el nombre el servicio:
  ```py
  r = requests.post("http://hasher/",
  ```
- Ir a dockercoins y ejecutar el docker compose:
  ```bash
  docker-compose up
  ```

## 9. Instalando nuestro primer cluster con Minikube, Kubeadm (14:58)
Minikube es una herramienta para desplegar un cluster en tu máquina local.

kubeadm es un boostrap, un utilitario que permite realizar todo lo mostrado en el repositorio de Kelsey.

Links:
GitHub - kelseyhightower/kubernetes-the-hard-way: Bootstrap Kubernetes the hard way on Google Cloud Platform. No scripts. -> https://github.com/kelseyhightower/kubernetes-the-hard-way
GitHub - kubernetes/minikube: Run Kubernetes locally -> https://github.com/kubernetes/minikube

Minikube: Utilitario que genera un cluster Master para levantar kubernetes
- Instalación: https://minikube.sigs.k8s.io/docs/start/linux/
  ```bash
  curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_1.7.2-0_amd64.deb \
 && sudo dpkg -i minikube_1.7.2-0_amd64.deb 
  ```
- Chequea que el sistema tenga habilitada la virtualización:
  ```bash
  egrep -q 'vmx|svm' /proc/cpuinfo && echo yes || echo no
  ```
- Si responde no, ir al BIOS y habilitarlo
- Iniciar minikube la primera vez (hay que especificar el driver de virtualizacion)
  ```bash
  minikube start --vm-driver=virtualbox
  ```
- Iniciar minikube en otro claster:
  ```bash
  minikube start -p
  ```
- Estado de minikube:
  ```bash
  minikube status
  ```
- Para interactuar con el cluster, se utilizar kubectl
- Corroborar el funcionamiento del cluster:
  ```bash
  kubectl get nodes
  ```

## 10. Instalando nuestro primer cluster con Kubeadm (11:44)
Play with Kubernetes -> https://labs.play-with-k8s.com/

## 11. Desplegando el Cluster en AWS con EKS (15:42) (Verlo otra vez y seguirlo paso a paso para hacerlo y actualizar los pasos)
Getting Started with Amazon EKS - Amazon EKS -> https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html

Te permite no tener que adminsitrar los cluster, Amazon permite desplegar un cluster en Amazon:
- Crear cuenta en aws.amazon.com
- Entrar en la url: console.aws.amazon.com
- Iniciar sesión
- Antes de crear un cluster, se tiene que tener creado un rol: https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html:
  - Abrir la console de amazon:  https://console.aws.amazon.com/iam/
  - Elegir Roles -> Create role
  - Elegir EKS de la lista de servicios, luego EKS para el caso de uso, y luego Next: Permissions
  - Elegir Next: Tags
  - (Opcional) Las etiquetas de IAM son pares de clave-valor que puede añadir a su rol. Para más información acerca de como usar tags en IAM, ver: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_tags.html
  - Elegir Next: Review
  - Para el Role name, ingresar un unico nombre para el rol, como por ejemplo "eksServiceRole", luego elegir Create role.
- Ir a Services -> EKS
- Crear nuevo cluster -> step 1: https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html
  - Cluster name: Nombre del cluster
  - Kubernetes version: Versión de kubernetes (usar default por las dudas)
  - Rolename: Nombre del rol, es obligatorio
  - VPC: Virtualnetwork (default)
  - Subnets: Que segmentos de red queremos que use amazon (dejamos default)
  - Security groups: Que puertos abiertos tiene que tener el cluster para comunicarse entre si (dejar default)
  - (El resto dejar todo por default), ver doc en: https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html
- Instalar kubectl para amazon EKS para usar desde nuestra consola los servicios de Amazon siguiendo el doc dependiendo el SO: https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html:
  - Linux (con curl)
    - Descargar la ultiva versión:
    ```bash
    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
    ```
    - Hacer de kubectl un binario ejecutable
    ```bash
    chmod +x ./kubectl
    ```
    - Mover el binario in el PATH
    ```bash
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```
    - Corroborar versión
    ```bash
    kubectl version --client
    ```
  - Linux (sin curl)
    ```bash
    sudo apt-get update && sudo apt-get install -y apt-transport-https \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/ kubernetes.list \
    sudo apt-get update \
    sudo apt-get install -y kubectl \
    ```
- Instalar autenticación: https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html
  - Descargar binario:
    ```bash
    curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
    ```
  - Aplicar permisos de ejecución al binario
    ```bash
    chmod +x ./aws-iam-authenticator
    ```
  - Copiar el binario al PATH
    ```bash
    mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$PATH:$HOME/bin
    ```
  - Agrega $HOME/bin al PATH
    ```bash
    echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc
    ```
  - Chequea que esté instalado correctamente
    ```bash
    aws-iam-authenticator help
    ```
- Instalar AWS CLI para gestionar configuraciones de Amazon: https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html:
  - Linux (comandos)
    - Ir a carpeta donde se quiere tener aws
    - Descargar el binario:
      ```bash
      curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
      ```
    - Descomprime el zip descargado:
      ```bash
      unzip awscliv2.zip
      ```
    - Ejecuta el instalador
      ```bash
      sudo ./aws/install
      ```
    - Chequear version
      ```bash
      aws --version
      ```
- Configurar credenciales de aws:
  - Crear Access Key y Secret access key:
    - Ir a https://console.aws.amazon.com/iam/home#/home
    - Usar usuario root:
      - Ir al nombre de usuario arriba a la derecha
      - Mis credenciales
      - Claves de acceso
      - Crear una clave de acceso, copiar lo que muestra y guardarlas
    - Crear nuevo usuario:
      - Ir a Usuarios
      - Crear nuevo usuario
      - Asignar un grupo
      - Ir al nombre de usuario y elegir Crear clave de acceso, aparecera por unica vez el ID y la clave secreta, copiarlas y guardarlas para la configuración de aws.
  - Iniciar configuración:
    ```bash
    aws configure
    ```
  - AWS Access Key ID [None]: ID de sesión
  - AWS Secret Acces Key [None]: Clave secreta
  - Default region name [None]: Nombre de region
  - Default output format [None]: Formato de output
- Se pueden crear cluster en aws desde comandos con el comando aws
- Crear kubeconfig file -> step 2: Archivo de configuración que permite que nuestro kubectl se comunique con el cluster de Amazon
  - Comando para crear 
  ```bash
  aws eks --region <region_del_cluster> update-kubeconfig --name <nombre_del_cluser>
  ```
  - Comando de ejemplo 
  ```bash
  aws eks --region us-east-2 update-kubeconfig --name plati-demo
  ```
  - Si tira un error similar a este:
    ```bash
    An error occurred (InvalidSignatureException) when calling the DescribeCluster operation: Signature not yet current: 20200305T232211Z is still later than 20200305T231706Z (20200305T231206Z + 5 min.)
    ```
    Es porque está mal la hora de la pc, se puede reconfigurar instalando la orden ntpdate:
    ```bash
    sudo apt install ntpdate
    ```
    Y luego ejecutar:
    ```bash
    sudo ntpdate ntp.ubuntu.com
    ```
  - Se pueden crear profiles para utilizar distintas cuentas a aws, agregando el parametro --profile al comando se especifica el profile:
    ```bash
    aws --profile <nombre_profile> eks --region <region_del_cluster> update-kubeconfig --name <nombre_del_cluser>
    ```
- Ejecutar kubectl para corroborar que se pueda conectar con el cluster
  ```bash
  kubectl get svc
  ```
- Lanzar y configurar los Workernodes -> step 3: https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html
  - En aws ir a Services -> CloudFormation (https://console.aws.amazon.com/cloudformation)
  - Elegir Create stack y luego With new resources (standard) / Template is ready
  - En Specify template elegir Amazon S3 URL
  - Pegar la URL que muestra el doc:
    ```bash
    https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-11-15/amazon-eks-nodegroup-role.yaml
    ```
  - En Specify stack details, ingresar un nombre del cluster platzi-demo y continuar
  - (Opcional) En la pagina de Configure stack options, se puede especificar los tags a usar
  - En la pagina Review, chequear el combo Capabilities y elegir Create stack
  - Cuando el stack es creado, seleccionarlo en la consola y elegir Outputs
  - Guardar el valor del NodeInstanceRole para el rol IAM que fue creado. Se necesitará cuando se cree un grupo de nodo.
  (visto hasta minuto 11:34)

## 12. Desplegando una aplicación de prueba con EKS

# Primer contacto con un cluster de kubernetes
## 13. Usando kubectl
Kubectl es la herramienta CLI para interactuar con el cluster de kubernetes, puede usarse para desplegar pods de pruebas, acceder a los contenedores y realizar algunos comandos como get nodes o get services

En .kube es donde se encuentra nuestro archivo config, la configuración de kubernetes.

kubectl get nodes: lista todos los nodos que tiene nuestro cluster
kubectl --config: puedes pasarle el archivo de configuración en caso de estar usando uno diferente.
kubectl --server --user: especificas la configuración sin necesidad de darle un archivo.
kubectl get nodes -a wide: muestra más datos de los nodos
kubectl describe nodes node1: da mucha información de ese nodo en especifico.
kubectl explain node: permite ver la definición de todo lo relacionado a ese nodo

## 14. Creación y manejo de pods

## 15. Deployments y replica sets

# Balanceo de carga y service discovery
## 16. Accediendo a nuestros PODS a través de servicios
El tipo de servicio en Kubernetes pueden ser de cuatro formas diferentes:
  Cluster IP: Una IP virtual es asignada para el servicio
  NodePort: Un puerto es asignado para el servicio en todos los nodos
  Load Balancer: Un balanceador externo es provisionado para el servicio. Solo disponible cuando se usa un servicio con un balanceador
  ExternalName: Una entrada de DNS manejado por CoreDNS

## 17. Enrutando el tráfico utilizando servicios

## 18. Desplegando nuestra app en k8s
Docker Hub -> https://hub.docker.com/u/dockercoins

# Escalando nuestra aplicación
## 19. Exponiendo servicios interna y externamente (kubectl-proxy)
kubectl-proxy es un proxy que corre foreground y nos permite acceder a la API de kubernetes de manera autenticada.

kubectl port-forward nos permite realizar lo mismo que kubectl-proxy, pero accediendo a cualquier puerto del servicio expuesto en nuestro cluster

## 20. Kubernetes dashboard
On Securing the Kubernetes Dashboard – Heptio -> https://blog.heptio.com/on-securing-the-kubernetes-dashboard-16b09b1b7aca?gi=b094972b89c3

## 21. Balanceo de carga y Daemon sets

## 22. Despliegues controlados

## 23. Healthchecks
Healthchecks es un organismo que tiene kubernetes para saber si nuestra aplicación está funcionando de una manera correcta para saber si debe removerla o reiniciarla al no estar en un estado deseable.

# Optimizando el uso de nuestro cluster
## 24. Gestionar stacks con Helm
Comandos utilizados en la clase:
```bash
// Instalar helm
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash

// Verificar si tenemos helm instalado
helm

// Configurar helm
helm init

// Verificar si Tiller está instalado
kubectl get pods -n kube-system

// Dar permisos a helm
kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default

// Buscar paquetes
helm search

// Ejemplo de cómo instalar un paquete
helm search prometheus
helm inspect stable/prometheus | less
helm install stable/prometheus --set server.service.type=NodePort --set server.persistentVolume.enabled=false

// Obtener servicios
kubectl get svc

// Crear helm chart
helm create dockercoins
cd dockercoins
mv templates/ templates-old
mkdir templates
cd ..
kubectl get -o yaml --export deployment worker
```

GitHub - helm/helm: The Kubernetes Package Manager -> https://github.com/helm/helm
Prometheus - Monitoring system & time series database -> https://prometheus.io/

## 25. Gestionando la configuración aplicativas utilizando Config Maps
Tenemos diferentes formas de configurar nuestras aplicaciones:
  Argumentos por línea de comandos
  Variables de entorno (env map en el spec)
  Archivos de configuración (config maps)
  – Guardan tanto archivo como valores clave/valor

## 26. Volúmenes
Un volumen nos va a permitir compartir archivos entre diferentes pods o en nuestro host. Estos se usan para que los archivos vivan a lo largo del tiempo y el pod pueda seguir haciendo uso de estos archivos de logs, archivos de configuración o cualquier otro.

Docker:
  Permiten compartir información entre contenedores del mismo host
  Permiten acceder a mecanismo de storage
  Docker config y docker secrets

Kubernetes:
  Permiten compartir información entre contenedores del mismo pod
  Permite acceder también a mecanismo de storage
  Se utilizan para el manejo de secrets y configuraciones

Ciclo de Vida
  El volumen se crea cuando el pod se crea.
  – Esto aplica principalmente para los volúmenes emptyDir.
  – Para otro tipo se conectan en vez de crearse.
  Un volumen se mantiene aún cuando se reinicie el contenedor.
  Un volumen se destruye cuando el pod se elimina.

# Autorización y Namespaces
## 27. Introducción a namespaces

## 28. Despliegue múltiples instancias de la misma aplicación en un solo cluster.
  Los namespaces no proveen aislación de recursos
  Un pod en un namespace A puede comunicarse con otro namespace B
  Un pod en el namespace default puede comunicarse con otro en el kube-system

## 29. Autenticación y autorización
Autenticación es el método por el cual Kubernetes deja ingresar a un usuario.
Autorización es el mecanismo para que un usuario tenga una serie determinada de permisos para realizar ciertas acciones sobre el cluster.

  Cuando el API server recibe un request intenta autorizarlo con uno o más de uno de los siguientes métodos: Certificados TLS, Bearer Tokens, Basic Auth o Proxy de autenticación.
  Si cualquier método rechaza la solicitud, se devuelve un 401.
  Si el request no es aceptado o rechazado, el usuario es anónimo.
  Por defecto el usuario anónimo no puede hacer ninguna operación en el cluster.

## 30. Service account tokens
Service Account Tokens es un mecanismo de autenticación de kubernetes y vive dentro de la plataforma, nos permite dar permisos a diferentes tipos de usuarios.

  Existen en la API de kubernetes. kubectl get serviceaccount
  Pueden crearse, eliminarse y actualizarse
  Un service account está asociado a secretos kubectl get secrets
  Son utilizados para otorgar permisos a aplicaciones y servicios

## 31. RBAC
Role based access control(RBAC) es un mecanismo de kubernetes para gestionar roles y la asociación de estos a los usuarios para delimitar las acciones que pueden realizar dentro de la plataforma.
  Un rol es un objeto que contiene una lista de rules
  Un rolebiding asocia un rol a un usuario
  Pueden existir usuarios, roles y rolebidings con el mismo nombre
  Una buena práctica es tener un 1-1-1 bidings
  Los Cluster-scope permissions permiten definir permisos a nivel de cluster y no solo namespace
  Un pod puede estar asociado a un service-account

# Fin del curso
## 32. Recomendaciones para implementar Kubernetes en tu organización o proyectos
Próximos pasos
  Establece una cultura de containers en la organización
  – Escribir Dockerfiles para una aplicación
  – Escribir compose files para describir servicios
  – Mostrar las ventajas de correr aplicaciones en contenedores
  – Configurar builds automáticos de imágenes
  – Automatizar el CI/CD (staging) pipeline

  Developer Experience: Si tienes una persona nueva, debe sentirse acompañada en este proceso de por qué usamos kubernetes y quieres mantener la armonía de ese proceso.

  Elección de un cluster de producción: Hay alternativas como Cloud, Managed o Self-managed, también puedes usar un cluster grande o múltiples pequeños.

  Recordar el uso de namespaces: Puedes desplegar varias versiones de tu aplicación en diferentes namespaces.

  Servicios con estados (stateful)
  – Intenta evitarlos al principio
  – Técnicas para exponerlos a los pods (ExternalName, ClusterIP, Ambassador)
  – Storage provider, Persistent volumens, Stateful set

  Gestión del tráfico Http
  – Ingress controllers (virtual host routing)

  Configuración de la aplicación
  – Secretos y config maps

  Stacks deployments
  – GitOps (infraestructure as code)
  – Heml, Spinnaker o Brigade

# Bonus
## 33. Clase en vivo: workflows de Kubernetes usando git
GitOps es una práctica que gestiona toda la configuración de nuestra infraestructura y nuestras aplicaciones en producción a través de Git. Git es la fuente de verdad. Esto implica que todo proceso de infraestructura conlleva code reviews, comentarios en los archivos de configuración y enlaces a issues y PR.
  Infraestructura como código
  Mecanismo de convergencia
  Uso de CI como fuente de verdad
  Pull vs Push
  Developers y operaciones(git)
  Actualizaciones atómicas

GitOps tomo tanta popularidad en la comunidad de DevOps por el impacto que genera.
  Poder desplegar features nuevos rápidos
  Reducir el tiempo para arreglar bugs
  Generar el sentimiento de control y empoderamiento. Confidencia y control
  20 deploys por día
  80% en ahorro del tiempo para arreglar errores en producción

GitHub - weaveworks/flux: The GitOps Kubernetes operator -> https://github.com/fluxcd/flux


