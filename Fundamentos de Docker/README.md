# Duración del curso: 03:07:02

# 1. Bienvenida al curso (03:12)
Da la bienvenida al curso. 
Muestra MURAL (pizzarron digital en la web), una pizzarra online a tiempo real.
www.mural.co

# 2. Problemáticas del desarrollo de software profesional (06:50)
A la hora de hacer aplicaciones y proyectos de software nos podemos encontrar con varios problemas, estos problemas los podemos agrupar en tres categorías:
  - Construir:
    - Dependencias de desarrollo
    - Versiones de entornos de ejecución
    - Equivalencia de entornos de desarrollo
    - Equivalencia con entornos productivos
    - Versiones/compatibilidad 3er party
  - Distribuir:
    - Output de build heterogeneo
    - Acceso a servidores productivos
    - Ejecución nativa vs virtualizada
    - Serverless
  - Ejecutar:
    - Dependencias de aplicación
    - Compatibilidad de sistema oeprativo
    - Disponibilidad de servicios externos
    - Recursos de hardware

Docker promete (y logra) ser la solución a todo nuestros problemas de una manera simple y sencilla. Con Docker se puede Contruir, distrubuir y ejecutar la aplicación de cualquier lado sin problemas.

# 3. Qué es Docker: containarization vs virtualization (09:31)
Docker permite resolver todos los problemas de contexto, permitiendo poder utilizar la aplicación desde cualquier computadora, ya sea para desarrollar, o utilizar.
## Maquinas virtuales:
- Pesadas
  - En el orden de los GB
  - Muchas VMs en el mismo host suelen repetirse en lo que contienen
- Adminsitración costosa
  - Una VM tiene que ser administrada como cualquier otra computadora: patching, updates, etc.
  - Hay que adminsitrar la seguridad interna entre apps
- Lentas
  - Correr nuestro codigo en una VM implica no solo arrancar nuestras aplicaciones, sino también esperar el boot de la VM en si
## Contenedores:
- Versátiles
  - En el orden de los MB
  - Tienen todas las dependencias que neceistan para funcionar correctamente
  - Funcionan igual en cualquier lado
- Eficientes
  - Comparten archivos inmutables con otros contenedores
  - Sólo se ejecutan procesos, no un OS completo
- Aislados
  - Lo que pasa en el container queda en el container
  - No pueden alterar su entorno de ejecución (a menos que explícitaente se indique lo contrario)

# 4. Instalación de Docker (10:45)
Vamos a ver cómo podemos instalar Docker en diferentes sistemas operativos, tendrás el enlace en los archivos de esta clase.
  - Para usuarios Mac o Windows pueden utilizar este enlace:

(Docker en Mac o Windows)[https://www.docker.com/]

Puedes comprobar que todo está funcionando, ingresa el comando “docker” en la terminal.
  - Para usuarios Linux, este es el enlace y seguir los pasos:
    [https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce]:
    - sudo apt-get update
    - sudo apt-get install \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg-agent \
      software-properties-common
    - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    - sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable"
    - sudo apt-get update
    - sudo apt-get install docker-ce docker-ce-cli containerd.io
    - sudo docker --version
    - Para que el usuario use docker sin poner sudo:
      - sudo usermod -aG docker <nombre_usuario>

links:
https://www.docker.com/products/docker-desktop
https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository


# 5. Primeros pasos: Hola mundo y Docker Engine (03:44)
Realicemos nuestro primer ‘Hola Mundo’ en Docker utilizando el comando:
```bash
docker run hello-world
```
La arquitectura de Docker funciona cliente - servidor y además utiliza ‘daemon’ al conectarse con los contenedores.
Cuando se ejecuta el comando "Docker" inicia el cliente que se conecta con el servicio de coker que es el que trabaja con los contenedores.

# 6. Contenedores (04:56)
Los contenedores son el concepto fundamental al hablar de docker. Un contenedor es una entidad lógica, una agrupación de procesos que se ejecutan de forma nativa como cualquier otra aplicación en la máquina host.

Un contenedor ejecuta sus procesos de forma nativa

Más detalles: 
https://itnext.io/chroot-cgroups-and-namespaces-an-overview-37124d995e3d

# 7. Explorar el estado de docker (08:44)
Vamos a conocer algunos trucos de nuestra consola para explorar el estado del docker

- Para listar todos los contenedores de Docker, utilizamos el comando :
```bash
docker ps -a
```
```bash
docker ps -aq
```
- Podemos inspeccionar un contenedor en específico utilizando:
```bash
docker inspect nombreDelContenedor
```
- Filtrar el resultado del inspect:
```bash
docker inspect nombreDelContenedor -f '{{ json .Config.Env}}'
```
- Renombrar contenedor:
```bash
docker rename nombreDelContenedor nuevoNombre
```
```bash
docker run --name nuevoNombre nombreDelContenedor
```
- Logs de outputs de contenedores:
```bash
docker logs nombreDelContenedor
```
- Eliminar contenedores:
```bash
docker rm nombreDelContenedor
```
```bash
docker rm $(docker ps -aq)
```

# 8. El modo interactivo (08:04)
Para que no inicie y cierre un contenedor, se le pasa el argumento -it despues de docker run para que se muestre de forma interactiva:
```bash
docker run -it ubuntu
```

# 9. Ciclo de vida de un contenedor (07:35)
El comando con que se corre el contenedor, sera el principal para determinar si un contenedor sigue activo o no. Cuando el primer proceso (PID) se elimine/termine, el contenedor se apagará
- Iniciar un contenedor de ubuntu y que quede abierto siempre
```bash
docker run ubuntu tail -f /dev/null
```
- Abrir un contenedor abierto existente:
```bash
docker exec -it <nombre_contenedor>
```
- Cerrar/matar el PID principal a un contenedor abierto existente:
```bash
docker kill <nombre_contenedor>
```
```bash
docker rm -f <nombre_contenedor>
```

# 10. Exponiendo contenedores al mundo exterior (08:30)
Los contenedores están aislados del sistema y a nivel de red, cada contenedor tiene su propia stack de net y sus propios puertos.

Debemos redirigir los puertos del contenedor a los de la computadora y lo podemos hacer al utilizar este comando:
```bash
docker run -d --name server -p 8080:80 nginx
```
Se le pasa el parametro -d (detached) para decir que el contenedor lo deje levantado, pero que retorne el control de la terminar.
Los puertos son: izquierda es mi host:derecha el del contenedor (8080:80)

# 11. Datos en Docker (10:33)
Como compartir datos fisicos de la pc host con el contenedor:
- Crear carpeta que usará el contenedor para alamcenar la información
  mkdir mongodata
- Crea contenedor de mongodb montandole la carpeta creada:
  docker run --name db -d -v "/home/galaeno/Escritorio/Gonzalo/GIT/GitLab/devops/Fundamentos de Docker/mongodata:/data/db" mongo
- Ingresa al contenedor para ejecutar una consulta
  docker exec -it db bash
  - Ingresa al cliente de mongo y ejecuta las consultas:
    mongo
    use platzi
    db.users.insert({name:"Gonzalo"})
    db.users.find()
    - Se ve: { "_id" : ObjectId("5e2f79200dd7ab8a6087326b"), "name" : "Gonzalo" }
    - No importa si se elimina el contenedor, los datos permanecen en la pc host
*Nota*: Los directorios están espejados, lo que se modifique en /home/galaeno/Escritorio/Gonzalo/GIT/GitLab/devops/Fundamentos de Docker/mongodata, se modificará en /data/db del contenedor y viceversa.

# 12. Datos con Docker: Volumes (09:00)
A pesar de que no es lo más divertido que tiene Docker, esta herramienta nos permite recuperar datos que podíamos dar por perdido.

Existen tres maneras de hacer permanencia de datos:
  - Bind mount: Lo visto en la clase anterior. El problema es que cualquier podría acceder a esos datos desde la pc host
  - Volume: La diferencia con bind mount es que docker es quien sabe donde van a estar los archivos y no el usuario quien el que dice donde van a estar.
  - tmpfs mount: Los archivos quedan en memoria, cuando se cierra el contenedor, se pierden.

Volume:
- Listar
  docker volume ls
- Borrar solo los volumenes que no se están usando
  docker volume prune
- Crear nuevo
  docker volume create dbdata
- Montar un volumen en un contenedor
  docker run -d --name db --mount src=dbdata,dst=/data/db mongo
- Ingresa al contenedor para ejecutar una consulta
  docker exec -it db bash
  - Ingresa al cliente de mongo y ejecuta las consultas:
    mongo
    use platzi
    db.users.insert({name:"Gonzalo"})
    db.users.find()
    - Se ve: { "_id" : ObjectId("5e2f79200dd7ab8a6087326b"), "name" : "Gonzalo" }
    - No importa si se elimina el contenedor, los datos permanecen en la pc host

Se puede conectar un volumen con un AWS, Azure, etc para guardar el contenido, no es necesario que si o si sea almacenamiento local.

Docuemntación sobre storage:
https://docs.docker.com/storage/

# 13. Conceptos fundamentales de Docker: imágenes (09:56)
Las imágenes son un componente fundamental de Docker y sin ellas los contenedores no tendrían sentido. Estas imágenes son fundamentalmente plantillas o templates.
Algo que debemos tener en cuenta es que las imágenes no van a cambiar, es decir, una vez este realizada no la podremos cambiar.

- Traer imagen que no se tiene, pero no la ejecuta:
  docker pull <imagen>
- Listar imagenes
  docker image ls
- Pedir versión de imagen especifica
  docker pull ubuntu:18.04

Muestra todas las imagenes disponibles:
https://hub.docker.com

# 14. Construyendo nuestras propias imágenes (09:36)
Vamos a crear nuestras propias imágenes, necesitamos un archivo llamado DockerFile que es la "receta" que utiliza Docker para crear imágenes.

*Es importante que el DockerFile siempre empiece con un "FROM" sino, no va a funcionar. *

El flujo para construir en Docker siempre es así:
```bash
Dockerfile -> build -> Imágen -> run -> Contenedor
```

- Docker file ejemplo.
```Dockerfile
FROM ubuntu

RUN touch /usr/src/hola-platzi
```
- Comando para contruir la imagen (en la ruta donde está el dockerfile):
  docker build -t ubuntu:platzi .
  - t es el talk que se va a usar, el nombre de la imagen, en este caso el nombre es: ubuntu con tag platzi (ubuntu:platzi)
- Comando para ejecutar la imagen recien creada
  docker run -it ubuntu:platzi
- Cambiar repositorio de la imagen creada para que este en uno propio y no en el de docker
  docker tag ubuntu:platzi <nombre_usuario>/ubuntu:platzi
- Login a docker antes de hacer push
  docker login
- Publicar una imagen a repositorio propio
  docker push galaeno/ubuntu:platzi

![Flujo 1](./Flujo 1.png)
![Flujo 2](./Flujo 2.png)
![Flujo 3](./Flujo 3.png)

# 15. Comprendiendo el sistema de capas (07:54)
- Historia de una imagen
  docker hsitory ubuntu:platzi

Herramienta para ver el historial de una imagen divido en capas:
https://github.com/wagoodman/dive

# 16. Usando docker para desarrollar aplicaciones (06:59)
En esta clase vamos a aplicar lo aprendido, y empezar a desarrollar con docker utlizando como base un proyecto desarrollado en node cuyo enlace lo puedes encontrar en los archivos del curso:
https://github.com/platzi/docker

Dockerfile
```Dockerfile
FROM node:8

COPY [".", "/usr/src/"]

WORKDIR /usr/src

RUN npm install

EXPOSE 3000

CMD ["node", "index.js"]
```

- Comando para que luego de correr el contenedor y éste se cierre, lo borre, para no acumular:
  docker run --rm -p 3000:3000 platziapp

# 17. Reto (00:35)
El reto es: hacer andar la aplicación en node v10

# 18. Entendiendo el cache de layers para estructurar correctamente tus imágenes (09:17)
Explica como usar el dockerfile para guardar en cache lo necesario.
Tomando como ejemplo el Dockerfile anterior:
```Dockerfile
FROM node:10

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install

COPY [".", "/usr/src/"]

EXPOSE 3000

CMD ["npx", "nodemon", "index.js"]
```

- Se ejecuta comando para iniciar el contenedor y junto con nodemon, queda el contenedor siempre escuchando cambios, y al montar el volumen dede una carpeta local, cualquier modificación que se haga en el disco local, hará que nodemon reinicie la aplicación
  docker run --rm -p 3000:3000 -v /home/galaeno/Escritorio/Gonzalo/GIT/GitLab/devops:/usr/src platziapp

# 19. Docker networking: colaboración entre contenedores (08:20)
Podemos conectar 2 contenedores de una manera fácil sencilla y rapida. Vamos a ver que con tan solo unos comandos tendremos la colaboración entre contenedores.

- Listar redes
  docker network ls
En general la que se usa es NAME => none
- Crear network
  docker network create --attachable platzinet
  - El attachable permite que otros contenedores puedan conectarse a la red
- Eliminar red
  docker network rm platzinet
- Crear contenedor de mongo
  docker run -d --name db mongo
- Conectar a una red un contenedor
  docker network connect platzinet db
- Crea la imagen de la aplicación
  docker run -d --name app -p 3000:3000 --env MONGO_URL=mongodb://db:27017/test platziapp
  - El parametro env es para indicar variables de entorno y coo el codigo tiene ua que es MONGO_URL, la asigna
  - En la url de mongo, aparece db, ese db es el contenedor. Si dos contenedores están asignados a la misma red, pueden verse, por lo tanto, se pueden usar de esa forma
- Conecta la app a la red creada
  docker network connect platzinet app

Modificado dockerfile
```Dockerfile
FROM node:10

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install

COPY [".", "/usr/src/"]

EXPOSE 3000

CMD ["npx", "nodemon", "index.js"]
```

# 20. Docker-compose: la herramienta todo-en-uno (08:38)
Docker compose es una herramienta que nos permite describir de forma declarativa la arquitectura de nuestra aplicación, utiliza composefile (docker-compose.yml). Basicamente es lo mismo que se hizo en clases anteriores manualmente, de forma automatica en base al archivo docker-compose-yml

Para linux se debe instalar docker-compose:
https://docs.docker.com/compose/install/#install-compose

docker-compose.yml de ejemplo:
```yml
version: "3"

services:
  app:
    image: platziapp
    environment:
      MONGO_URL: "mongodb://db:27017/test"
    depends_on:
      - db
    ports:
      - "3000:3000"
  db:
    image: mongo
```

- Ejecutar compose
  docker-compose up

links:
https://docs.docker.com/compose/compose-file/compose-versioning/
https://docs.docker.com/compose/startup-order/

# 21. Trabajando con docker-compose (06:41)
Muestra como usar docker-compose de manera simple.
- Detach el compose (corre el compose, sin output)
  docker-compose up -d
- Ver estado de docker-compose
  docker-compose ps
- Ver logs de compose
  docker-compose logs app
- Ingresar al contenedor
  docker-compose exec app bash
- Limpiar los servicios, contenedores, etc del compose
  docker-compose down

# 22. Docker-compose como herramienta de desarrollo (14:53)
Muestra como en general se usa docker-compose para desarrollo.
docker-compose.yml:
```yml
version: "3"

services:
  app:
    build: .
    environment:
      MONGO_URL: "mongodb://db:27017/test"
    depends_on:
      - db
    ports:
      - "3000-3010:3000" # Desde el puerto 3000 al 3010 de mi pc
    volumes:
      - .:/usr/src # Espeja la ruta del build a la del contenedor
      - /usr/src/node_modules # Esto dice que no modifique el node_modules del contenedor
  db:
    image: mongo
```
- Build docker-compose
  docker-compose build
- Decir a un contenedor que tenga mas de una instancia
  docker-compose scale app=4

# 23. Conceptos para imágenes produtivas (11:31)
Cuando se crea una imagen para usar en producción, se recomienda:
- Usar .dockerignore: Todos los archivos que no se quiere que esten en la imagen (funciona igual que el .gitignore)
- Usar multistage builds: Nos permite usar dockerfiles que tienen varias fases de build donde interactuan
  - Elegir docker file en el comando para development:
    docker build -t platziapp -f <nombre_archivo_dockerile>
  - Ejemplos de archivos de dockerfile:
    - development.Dockerfile:
      ```dockerfile
      FROM node:10

      COPY ["package.json", "package-lock.json", "/usr/src/"]

      WORKDIR /usr/src

      RUN npm install --only=production

      COPY [".", "/usr/src/"]

      RUN npm install --only=development

      EXPOSE 3000

      CMD ["npx", "nodemon", "index.js"]
      ```
    - production.Dockerfile
      ```dockerfile
      FROM node:10 as builder

      COPY ["package.json", "package-lock.json", "/usr/src/"]

      WORKDIR /usr/src

      RUN npm install --only=production

      COPY [".", "/usr/src/"]

      RUN npm install --only=development

      RUN npm run test


      # Productive image
      FROM node:10

      COPY ["package.json", "package-lock.json", "/usr/src/"]

      WORKDIR /usr/src

      RUN npm install --only=production

      COPY --from=builder ["/usr/src/index.js", "/usr/src/"]

      EXPOSE 3000

      CMD ["node", "index.js"]
      ```

# 24. Manejando docker desde un contenedor (09:42)
Vamos en esta clase a ver uno de los secretos mejor guardados de docker. Vamos a ver como manejar un contenedor desde otro contenedor. Es una manera muy interesante para aprovechar la potencia de docker y todas las ventajas que ofrece.

Concepto docker in docker, manejar contenedores en contenedores.
- Comando para usar el contenedor de docker
  docker run -it -rm -v /var/run/docker.sock:/var/run/docker.sock docker:18.06.1-ce
  Se usa para poder generar builds sin tener que configurar localmente cosas

link:
https://hub.docker.com/_/docker/

# 25. Cierre del curso (01:20)
Cierre
